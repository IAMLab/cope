//******************************** Global variables
var OS;///it can return MacIntel, MacPPC, Win32, Linux i686 but we transform into Mac, Win or Lin 
var VERSION="05";
var start;var start_t1;var session_started;var session_ended; 
var nots_prompted=0;var nots_responded=0;
var RETRACING_THRHLD=2;//T4. Number of pages that construct a pattern
var SHELTERS_THRHLD=5;//T3. Number of sites that are considered a shelter
var TIME_THRHLD=60000;//T8. Number of maximum milliseconds to consider a restart
var GU_TIME_THRHLD=180000;//T9. Number of maximum milliseconds to consider a giving up
var NOP_THRHLD=60000;//T6. Number of maximum milliseconds to consider no operation
var PAGE_THRHLD=2;//T2 and T7. Number of pages considered in a pattern. If equals 2 --> A B A C A; if 3 --> A B A C A D A , and so on
var NUMBER_OF_WORDS=null;//number of words in a page
var WPS=10;//maximum number of words that can be scanned per second --> 600 per minute
var CORR_VALUE=8;//a ratio to correct WPS as users will not read the whole page. Following a F-shaped or triangular pattern users scan half of a page thus CORR_VALUE=2. It is plausible to think that in this triangle only half of the half content is scanned; thus CORR_VALUE=4 
var CORR_VALUE_H=1;//feel inclined to put it to 1.5
var SERVER_TACTICS="http://130.88.193.240/COPEserv/servlet/Bridge";
var SERVER_STATS="http://130.88.193.240/COPEserv/servlet/Stats";
var SERVER_MODE="http://130.88.193.240/COPEserv/servlet/Mode";
var currentURL; 
var event_panel; //get a reference to the label of each row in the lateral panel
var status=true; // status of the system
var system_status;// the button to change the status
var arrival_mode;//{linked, direct, updated or tabbed} check how it goes with multiple navs
var landing_mode=new Array();
//global variables for t1, overcoming info overload
var header_text=false;var ctrlf=false;var latest_key=null;var bottom_reached_t1=false;
var initial_vlocation;var web_page_length;var page_ratio;var first_time;var ratio_of_distance_to_bottom=1;var xcroll=false;
var deactivatet1=false;
//global variables for t4, retracing
var url_array = new Array();
var index_of_pattern_B =-2;index_of_pattern_A=-2;
var index=-1;
var lengthofpattern=1;
var last_title;
//variables for t4.2
var pattern_size=0;
//variables for t6
var timeIN=0;var timeONTEXT=0; var time_over_text=0;var inside_text=false;
//global variables for t5 (persevering) and t7 (trial and error)
var history_array=new Array();
var time_in_wp=new Array();
var traversal_array=new Array();//true if linked false otherwise
var titles_array=new Array();
//unique identifier of user at DB
var userID;
var mainwindow;var tabcontainer;
var last_tactic=-1; var last_tactic_index=-1;
last_tactic_index_t2=-1;last_tactic_index_t5=-1;last_tactic_index_t7=-1;
var locale;
var last_t_added_URL; //to control the conflict between unloads and tab closes
var index_closed_tab;
var t1_triggered=false;var t6_triggered=false;
var hasfocus=true;
var mode=-1;//a tinyint retrieved from the server that denotes the way the application is going to run
var urlsunloaded=0;var nav_time;//latest navigation time
var firstload=false;firstinit=false;
// ******************************** System scripts
//get a reference to browser from a sidebar environment
mainwindow = window.QueryInterface(Components.interfaces.nsIInterfaceRequestor)
                   .getInterface(Components.interfaces.nsIWebNavigation)
                   .QueryInterface(Components.interfaces.nsIDocShellTreeItem)
                   .rootTreeItem
                   .QueryInterface(Components.interfaces.nsIInterfaceRequestor)
                   .getInterface(Components.interfaces.nsIDOMWindow); 
var gBundle = Components.classes["@mozilla.org/intl/stringbundle;1"].getService(Components.interfaces.nsIStringBundleService);
locale = gBundle.createBundle("chrome://webtactics/locale/wt.properties");

//get the tabs container
tabcontainer = mainwindow.gBrowser.tabContainer;  
tabcontainer.addEventListener("TabSelect", tabselected, false);
tabcontainer.addEventListener("TabClose", tabclosed, false);

//add the listener to the window:load
window.addEventListener("load", function load(event){ 
    window.removeEventListener("load", load, false); //remove listener, no longer needed  
    webtactics.init();
},false);

//add closing listener to sidebar
window.addEventListener("beforeunload", function(e) { webtactics.onunload(e); }, false);
window.addEventListener("unload", function(e) { webtactics.onunload(e); }, false);

var webtactics = {  
    init: function() {
		mainwindow.gBrowser.addEventListener("pageshow", this.onwpload, false);
	},  
    onwpload: function(aEvent){
		if (status){
			var doc = aEvent.originalTarget; // doc is document that triggered the event  
			var win = doc.defaultView; // win is the window for the doc
			var browser=mainwindow.gBrowser.getBrowserForDocument(doc);
			//the initial tab is not selected so we have to simulate its selection
			if ((win!=null) && (doc.location!=null)){
				if ((win == win.top) && (doc.location.href==window.content.document.URL)){
					if ((history_array.length==1 || history_array.length==0) && !firstload) {
						browser.setAttribute("selected","true");
						firstload=true;firstinit=true;
					}
					if (browser.hasAttribute("selected")) {
						arrival_mode="direct";
						init();
					}
				}
			}
		}
	},
	onunload: function(e){
		if (status) {
			t8_t9_quit();			
		};
	}
};

function tabselected(event) {
	if (status){
		var browser = mainwindow.gBrowser.getBrowserForTab(event.target);
		browser.setAttribute("selected","true");
		if	(index_closed_tab==history_array.length){
		}
		else{
			time_management();//"close" the previous one that has not been manually closed
		}
		if (window.content.document.URL.substring(0,5)!="about"){ 
			arrival_mode="tabbed";
			init();
		}
		else {
			currentURL=window.content.document.URL;
			history_array.push(currentURL);
			titles_array.push(content.document.title);
			start=Date.now();
			start_t1=Date.now();
		}
	}
}

//problems if page opened, not accessed and closed-->check whether is in history. Problems if two pages are open with same url
//this is because time_management, which is called at closing, updates some arrays
function tabclosed(event) {
	if (status){
		var browser = mainwindow.gBrowser.getBrowserForTab(event.target);
		if	(last_t_added_URL!=browser.contentDocument.URL){//the one unloaded is not the active tab-->remove from vector
			if (browser.hasAttribute("selected")){
				time_in_wp.splice(time_in_wp.length-1,1);
			}
		}
		else{
		index_closed_tab=history_array.length;
		}
	}
}
 
function changestatus(){
	system_status = window.document.getElementById("activity");
	if (!status){//start
		system_status.setAttribute("label",locale.GetStringFromName("status.running"));
		system_status.setAttribute("image","chrome://webtactics/skin/green.png");
	   	status= !status;
		manageUserId();
		session_started=Date.now();
		var desc_display=window.document.getElementById("desc");
		desc_display.setAttribute("value", locale.GetStringFromName("in"));
		init();
	}
	else{
		session_ended=Date.now();
		submit_session();
		time_management();
		//dealing with the interface
		system_status.setAttribute("label",locale.GetStringFromName("status.stop"));
		system_status.setAttribute("image","chrome://webtactics/skin/red.png");
		//removing elements on the panel but the display
		var groupbox=window.document.getElementsByTagName("groupbox");
		for (var g=0; g<groupbox.length;g++){
			if (groupbox[g].getAttribute("id")!="display")
				groupbox[g].parentNode.removeChild(groupbox[g]);
		}
		var desc_display=window.document.getElementById("desc");
		desc_display.setAttribute("value", locale.GetStringFromName("out"));
		//statistical data updated for the next session
		nots_prompted=0;nots_responded=0;
		//reset global variables
		index_of_pattern_A =-2;index_of_pattern_B =-2;index=-1;lengthofpattern=0;
		url_array=[];history_array=[];time_in_wp=[];traversal_array=[];landing_mode=[];titles_array=[];
		last_tactic=-1;last_tactic_index_t2=-1;last_tactic_index_t5=-1;last_tactic_index_t7=-1;
		last_tactic_index=-1;urlsunloaded=0;mode=-1;
		status=!status;
	}
}

function get_system_modality(){
	mode=undefined;
	var requestObject = new XMLHttpRequest();
	requestObject.open("GET",SERVER_MODE,false);//synchronous request
	requestObject.send(null);
	mode=requestObject.responseText;
	return mode;
}

function init(){
	if(status){
		//obtain the way in which the tool operates
		/*KEY for modes
	     * 0: feedback; 1: test mode; 2: silent; 3: destroy
	     */		
		if (mode==-1){//if no mode is assigned means that the program is running in a default mode and some varibles should be initialised
			session_started=Date.now();
			manageUserId();
			mode=get_system_modality();
			// (mode==0 || mode==1){//operate intrusively
			if (mode==2 || mode==3){//operate silently
				window.document.getElementsByTagName("page")[0].style.setProperty("display","none");
				var menuitems=window.document.getElementsByTagName("menuitem");
				for (var mi=0; mi<menuitems.length;mi++){
					var label=menuitems[mi].getAttribute("label");
					if (label=="WebTactics"){
						if (mode==2){
							menuitems[mi].setAttribute("disabled","true");
						}
						else if (mode==3){
							menuitems[mi].setAttribute("hidden","true");
						}
					}
				}
				if (mode==3){//kill
					alert(locale.GetStringFromName("mode3"));
				}
			}			
		}
		if (mode!=3){
			//Detect browser and OS
			OS = navigator.platform;//MacIntel, MacPPC, Win32, Linux i686
	    	OS = OS.substring(0,3);//Mac, Win, Lin
	    	
	    	//general initialisations
	    	start=Date.now();var redirected=false;
	    	
	    	//initialisation for t1
			header_text=false;ctrlf=false;latest_key=null;header_text=false;initial_vlocation=window.content.scrollY;
			start_t1=Date.now();bottom_reached_t1=false;ratio_of_distance_to_bottom=1;xcroll=false;deactivatet1=false;
	    	//init for t6
	    	timeIN=0;timeONTEXT=0;time_over_text=0;inside_text=false;//the latter to avoid blocking situations
	    	//vertical distance to the bottom
	    	web_page_length= window.content.document.documentElement.scrollHeight - (window.content.document.documentElement.clientHeight + initial_vlocation);
	    	//check the percentage of remaining page    	
	    	if (web_page_length>0){
	    		var remaining_ratio=web_page_length/window.content.document.documentElement.scrollHeight;
	    		if (remaining_ratio <= 0.20){
	    			deactivatet1=true;
	    		}
	    	}
	 		//is the bottom visible from outset
	    	else if (web_page_length==0){
				deactivatet1=true;
	    	} 
	 		t1_triggered=false;t6_triggered=false;				
			window.content.onfocus=focused;
			window.content.onblur=unfocused; 
			window.document.onblur=unfocused; 
			mainwindow.document.getElementById("viewWTbar").setAttribute("delayed","false");//in case there are conflicts between different ways of opening	
			event_panel=window.document.getElementById("eventpanel");
			//clean interface at every page but for the display
			var groupbox=window.document.getElementsByTagName("groupbox");
			for (var f=0; f<groupbox.length;f++){
				if (groupbox[f].getAttribute("id")!="display")
					groupbox[f].parentNode.removeChild(groupbox[f]);
			}		
			//----- START history array management
			window.content.onhashchange=sim_init;//check whether there is a page update without an onload event;sterotypical in AJAX web pages
			if (window.content.location.toString()!=window.content.document.URL)
				currentURL=window.content.location.toString();		
			else
				currentURL=window.content.document.URL;
			history_array.push(currentURL);
			if (history_array.length-2==urlsunloaded){//one page in between eg: google
				history_array.splice(history_array.length-2, 1);//remove the intermediate page
				titles_array.splice(titles_array.length-1, 1);//remove the title
				landing_mode.splice(landing_mode.length-1, 1);//landing mode and traversal array are the ones triggered on redirections
				traversal_array.splice(traversal_array.length-1, 1);
				redirected=true;
			}
			titles_array.push(content.document.title.toLowerCase());
	    	first_time=isVisited();
			//if the page wasn't linked there is no value added to the array so now we have to add the false value
			if (redirected){//redirected through clicking on a link
				arrival_mode="linked";traversal_array.push(true);}			
			else if	(history_array.length!=traversal_array.length)//redirected through not clicking 
				traversal_array.push(false);
			else if (arrival_mode!="tabbed" && arrival_mode!="updated") 
				arrival_mode="linked";
			landing_mode.push(arrival_mode);		
			manage_transitions();//add listeners to all links
			manage_t6();//add listeners to block elements
			NUMBER_OF_WORDS=visiblewords();
			if (header_text) page_ratio=WPS*CORR_VALUE_H;
			else page_ratio=WPS*CORR_VALUE;
			//----- END history array management
			
			// START----- tactics management	
			if (firstinit){ 
				t8_t9_check();
				firstinit=false;
			}
			t2();t3();t4();t5();t6();t7();
			window.content.onscroll=t1;
			mainwindow.onkeydown=listentoFind;
			window.content.onbeforeunload=time_management;//executes after each page is closed			
		}
			//END----- tactics management
	}
}

//----------------------------------------------------
function focused(){
		hasfocus=true;
}
function unfocused(){
		hasfocus=false;
}

function sim_init(){
	if (status){
		time_management();
		arrival_mode="updated";//this location is important: after time_management
		var browser=mainwindow.gBrowser.browsers[0];
		if (history_array.length==1) browser.setAttribute("selected","true");
		init();
	}
}
function manage_transitions(){
	if (status){
		var link_list= window.content.document.getElementsByTagName("a");
		for (var j=0; j<link_list.length;j++)
			link_list.item(j).addEventListener("click", clicked, false);
	}
}

function clicked(){
	if (status) traversal_array.push(true);
}

// ******************************** Tactics
function t1(){
	if (status && !t1_triggered && !deactivatet1) t1_overcoming_information_overload();
}
function t2(){
	if (status) t2_asking_for_help();
}
function t3(){
	if (status) t3_sheltering();
}
function t4(){
	if (status){ 
		t4_retracing();
		t4_retracing_backtrack();
	}
}
function t5(){ 
	if (status) t5_rechecking();
}
function t6(){
	if (status && !t6_triggered) t6_nop();
}
function t7(){
	if (status) t7_trialanderror();
}
function t8_t9_check(){
	if (status) t8_t9_restarting_checking();
}
function t8_t9_quit(){
	if (status) t8_t9_restarting_quiting();
}
//=======================================================
function t1_overcoming_information_overload(){
	var ratio;var elapsed;var end;
	if (window.content.scrollX>0 && !xcroll) xcroll=true;
	//if current distance is the bottom and it's been reached for the first time
	if ((window.content.scrollY >= web_page_length) && first_time && (!bottom_reached_t1)){ 
		end=Date.now();
		elapsed= (end-start_t1)/1000;
		elapsed=elapsed.toFixed(2);
		/*if the page was already scrolled as soon as it loaded we get 
		the ratio of page that was scrolled in order to estimate the number
		of words in the remaining*/
		if (initial_vlocation>0){
			ratio_of_distance_to_bottom=1-(initial_vlocation/window.content.document.documentElement.scrollHeight);
			NUMBER_OF_WORDS=NUMBER_OF_WORDS*ratio_of_distance_to_bottom;
		}
		ratio=NUMBER_OF_WORDS/elapsed; //words per second metric
		ratio=ratio.toFixed(0);
		if (ratio > page_ratio){//times 4 b/c we assume that only 1/4 of a page it'll be read
			if (!ctrlf) bottom_reached_t1=true;//if it is not the consequence of Ctrl+F it means bottom has been reached abruptly
		}
		start_t1=Date.now();
	}
	else if(bottom_reached_t1 && (window.content.scrollY==0)){
		t1_triggered=true;
		if (xcroll){
			notify("1.1");
		}
		else{
			notify("1.2");
		}
	}
} 

function listentoFind(e){
	if (OS=="Win" || OS=="Lin"){//look for Ctrl
		if (e.ctrlKey && e.keyCode==70){
			ctrlf=true;
		}
	} 
	else if (OS=="Mac")//look for cmd
		if (latest_key==224 && e.keyCode==70){
			ctrlf=true;
		}
	latest_key=e.keyCode;
}

function isVisited(){
	Components.utils.import("resource://gre/modules/Services.jsm");
	Components.utils.import("resource://gre/modules/FileUtils.jsm");
	var file = FileUtils.getFile("ProfD", ["places.sqlite"]);
	var dbConn = Services.storage.openDatabase(file); // Will also create the file if it does not exist
	var statement = dbConn.createStatement("SELECT * FROM moz_places WHERE url=='"+currentURL+"'");
	var nvisits=0;
	try{
		while (statement.executeStep()) {  
			nvisits=statement.row.visit_count;
		}
	}
	finally{
		statement.reset(); 
	}
	if (nvisits=="1") {
		return true;
	}
	else {return false;}	
}
//=======================================================
function t2_asking_for_help(){//checks the following traversal pattern A1 B A2 C A3. 
	if (history_array.length>=((PAGE_THRHLD*2)+1)){
		var sizet2=history_array.length-1;
		if (history_array[sizet2]==history_array[sizet2-2]){//A3==A2?
			if (history_array[sizet2-2]==history_array[sizet2-4]){//A2==A1?
				if (history_array[sizet2-1]!=history_array[sizet2-3]){//C!=B?
					if (history_array[sizet2-1]!=history_array[sizet2]){//C!=A?
						if (history_array[sizet2-3]!=history_array[sizet2]){//B!=A?
							if (!(time_in_wp[time_in_wp.length-1]) && !(time_in_wp[time_in_wp.length-3])){//check whether B and C were not scans 
								//latest enacted t2 is at least 4 items away in the history
								if ((history_array.length>=last_tactic_index_t2+4) && (history_array.length>=last_tactic_index_t5+3)){
									if (!is_it_tabbed(4)){//non-tabbed
										if (sameDomain()){
											notify("2.1");
										}
										else if (overlappingTitle()){
											notify("2.2");
										}
										else{
											notify("2.3");
										}
									}
									else{//tabbed
										if (sameDomain()){
											notify("2.4");
										}
										else if (overlappingTitle()){
											notify("2.5");
										}
										else{
											notify("2.6");
										}
									}
									last_tactic_index_t2=history_array.length;
								}
							}
						}
					}
				}
			}
		}
		 
	}
}
/*KEY
 * 
 * 2.1: non-tabbed pages same domain 
 * 2.2: non-tabbed pages distinct domain, overlapping titles
 * 2.3: non-tabbed pages distinct domain  
 * 2.4: tabbed pages same domain
 * 2.5: tabbed pages distinct domain, overlapping titles 
 * 2.6: tabbed pages distinct domain
 * 
 * */
function is_it_tabbed(size){//useful for T2 and T7. It checks that T2 and T7 haven't been triggerd through tabbing. 
	var tabbed=false;
	for (var i=landing_mode.length-size;i<=landing_mode.length-1;i++){
		if (landing_mode[i]=="tabbed"){
			tabbed=true;
		}
	}
	return tabbed;
}	
//=======================================================
function t3_sheltering(){//improveable by accessing to the DB and getting the frecency
	//https://developer.mozilla.org/en/Querying_Places
	if (history_array.length>SHELTERS_THRHLD){// check it does not happen in the first run
		Components.utils.import("resource://gre/modules/Services.jsm");
		Components.utils.import("resource://gre/modules/FileUtils.jsm");
		var file = FileUtils.getFile("ProfD", ["profile.sqlite"]);
		var dbConn = Services.storage.openDatabase(file); // Will also create the file if it does not exist
		var statement = dbConn.createStatement("SELECT url FROM historyWT ORDER BY keyWT DESC LIMIT "+SHELTERS_THRHLD);		
		var shelterfound = false;var url_tmp;
		try{
			while (statement.executeStep() && !shelterfound) {  
				url_tmp=statement.row.url;
				if (url_tmp==currentURL) shelterfound=true;//if the current URL is considered a shelter according to our criteria 
			}
		}
		finally{statement.reset();}
		
		if (shelterfound){
			//check whether the previous task was a tactic and it was not 3.1
			if ((last_tactic_index+1==history_array.length) && (last_tactic!="3.1")){ 		
				notify("3.1");
			}
		}
	}
}
//=======================================================
function t4_retracing(){// the longest traversed path which is repeated. Turning points can be identified.
	index=index+1;//index starts on 0
	if (url_array.length>0){
			var tmp_lastindex=url_array.lastIndexOf(currentURL);
			//if there is not a match, which is earlier than the current pattern but breaks a more recent pattern AND
			//there is a match, its not the first element on the array
			if ((tmp_lastindex>index_of_pattern_A) && (tmp_lastindex!=-1)){
					lastindex=tmp_lastindex;
					//was the previous a pattern?
					if (index_of_pattern_B+1==index){
							//check backwards if this current item can be added to the pattern
							var isapattern=true;var istabbed=false;
							var tmp=lengthofpattern;
							if (tmp > 0){ 
									while ((tmp >0) && isapattern){
											if (url_array[lastindex-tmp]==url_array[url_array.length-tmp]){
												isapattern=true;
												//when the first element of each pattern is checked these values are the same
												if (tmp!=lengthofpattern){
													if (landing_mode[lastindex-tmp]=="tabbed" || landing_mode[url_array.length-tmp]=="tabbed"){
														istabbed=true;
													}
												}
												index_of_pattern_A=lastindex;
											}
											else{
												isapattern=false;
											}
											tmp=tmp-1;
									}
							}	
							if (isapattern){//it is a continuation of the pattern, so variables are updated
								if (istabbed){//is a pattern that contains a tab?
									index_of_pattern_A=-2;index_of_pattern_B=-2;lengthofpattern=1;		
								}
								else{									
									last_title=currentURL;index_of_pattern_B=index;lengthofpattern=lengthofpattern+1;
								}
							}
							else{//there is a match but  it is not the continuation of a pattern. The previous pattern is released 
								if (lengthofpattern>=RETRACING_THRHLD){
									//check there is only one item between patterns: Good-->A B C A B D; Bad-->A B C D A B D C  
									if ((index_of_pattern_A+1)==(index-1-lengthofpattern)){
										notify("4.1");
									}
								}
								index_of_pattern_A=tmp_lastindex;index_of_pattern_B=index;lengthofpattern=1;								
							}		
					}
					//the previous was not a pattern but there is a match
					else{
						lengthofpattern=1;index_of_pattern_A=lastindex;index_of_pattern_B=index;
					}
		}// there is no match OR the match is previous this pattern
		else if (index_of_pattern_B+1==index){
				if (lengthofpattern>=RETRACING_THRHLD){		
					if ((index_of_pattern_A+1)==(index-1-lengthofpattern)){
						notify("4.1");
					}
				}
				if (tmp_lastindex!=-1){//there is a previous match
					index_of_pattern_A=tmp_lastindex;index_of_pattern_B=index;lengthofpattern=1;
				}
				else{
					index_of_pattern_A=-2;index_of_pattern_B=-2;lengthofpattern=0;
				}
		}
	}
}

function t4_retracing_backtrack(){
	var tmp_lastindex=url_array.lastIndexOf(currentURL);
	//[A B1 C D E F G F E D C B2 C D E F H]
	//1. There is a match
	if (url_array.length>0 && tmp_lastindex!=-1){
		//1.1. Check the lastest match and check if it is adjacent to previous pattern.
		var distance=(pattern_size+1)*2;
		if (tmp_lastindex==index-distance){
			pattern_size++;
		}
		//1.2 There was a pattern and the current element belongs to that pattern but it is not extending it.
		else if(pattern_size>0){
			if (check_pattern()){
				notify("4.2");
				pattern_size=0;
			}
			else if(currentURL==url_array[index-2]){//at distance of 2 there's a match
				pattern_size=1;				
			}
			else{
				pattern_size=0;
			}
		}
	}
	//2. There is no match and there is a pattern 
	else if (url_array.length>0 && tmp_lastindex==-1 && pattern_size>0){
		if (check_pattern()){
			notify("4.2");
		}		
		pattern_size=0;
	}
	url_array.push(currentURL);//add current URL to array
}

function check_pattern(){
	//2.1 check whether current item is different from turning point [H!=G]
	var turning_point=index-((pattern_size*2)+2);//get index(G)
	if (currentURL!=url_array[turning_point]){
		//2.2 Get the indexes for the start of the pattern: B1 and B2
		var index_P1=turning_point-(pattern_size+1);
		var index_P2=turning_point+(pattern_size+1);
		var index_P=0;var pattern=true;
		while(index_P<=pattern_size){
			if (index_P==0){//first item needs a particular case
				if ((url_array[index_P1+index_P]!=url_array[index_P2+index_P])||(landing_mode[index_P2+index_P]=="tabbed")){
					pattern=false;
				}	
			}
			else{
				if ((url_array[index_P1+index_P]!=url_array[index_P2+index_P])||(landing_mode[index_P1+index_P]=="tabbed")||(landing_mode[index_P2+index_P]=="tabbed")){
					pattern=false;
				}	
			}			
			index_P++;
		}
		if (pattern){
			//if still true check the landing mode of remaining items [G-B2)
			for (var ijk=turning_point;ijk<index_P2;ijk++){
				if (landing_mode[ijk]=="tabbed"){
					pattern=false;
				}
			}
			//check landing mode of last item
			if (landing_mode[index]=="tabbed"){
				pattern=false;
			}
		}
		return pattern;
	}
	else return false;
}
	
//=======================================================
function t5_rechecking(){//checks the following traversal pattern A1 B1 A2 B2
	if (history_array.length>=4){
			var sizet5=history_array.length-1;
			if (!is_it_tabbed(3)){	
				if (history_array[sizet5]==history_array[sizet5-2]){//B2==B1?
					if (history_array[sizet5-1]==history_array[sizet5-3]){//A2==A1?
						if (history_array[sizet5-1]!=history_array[sizet5]){//A!=B?
							//latest enacted t5 is at least 4 items away in the history. See a guide for this in the COPE diary, page 99
							if ((history_array.length>=last_tactic_index_t5+2) && (history_array.length>=last_tactic_index_t7+2) && (history_array.length>=last_tactic_index_t2+2)){							
									if (!traversal_array[sizet5-1] && !traversal_array[sizet5]){ //if A2 and B2 are not reached through links but using browser controls or typing
										notify("5.1");//deliberated hard recheck
									}
									else if (!traversal_array[sizet5-1] && traversal_array[sizet5]){// backwards through control forward linked
										notify("5.2");//deliberated soft recheck
									}
									last_tactic_index_t5=history_array.length;
							}							
						}
					}
				}
			}	
		}
}
function t5_requerying(){
}
//=======================================================
function t6_nop(){
	window.content.onclick=nop;
	window.content.ondblclick=nop;
	window.content.onkeydown=nop;
	window.content.onkeypress=nop;
	window.onclick=nop;
	window.ondblclick=nop;
	window.onkeydown=nop;
	window.onkeypress=nop;
}
function nop(){
	if (status && hasfocus){
		time_over_text=0;
	}
}

function blockIN(){
	if (status && !t6_triggered && !inside_text){
		inside_text=true;
		timeIN=Date.now();
	}
}

function blockOUT(){
	if (status && !t6_triggered && timeIN!=0 && inside_text){
		inside_text=false;
		timeONTEXT=Date.now()-timeIN;
		if (timeONTEXT<5000){
			time_over_text=time_over_text+timeONTEXT;
			if ((time_over_text>NOP_THRHLD)){
				t6_triggered=true;
					notify("6.1");
			}
		}
		else if (timeONTEXT>30000){//penalise long hovers
			time_over_text=0;
		}
		
	}
}

function manage_t6(){
	if (status && !t6_triggered){
		var p_list= window.content.document.getElementsByTagName("p");
		for (var i=0; i<p_list.length;i++){
			if (!any_img_within(p_list.item(i))){
				p_list.item(i).addEventListener("mouseover", blockIN, false);
				p_list.item(i).addEventListener("mouseout", blockOUT, false);
			}
		}
		var h1_list= window.content.document.getElementsByTagName("h1");
		for (var i=0; i<h1_list.length;i++){
			if (!any_img_within(h1_list.item(i))){
				h1_list.item(i).addEventListener("mouseover", blockIN, false);
				h1_list.item(i).addEventListener("mouseout", blockOUT, false);
			}
		}
		var h2_list= window.content.document.getElementsByTagName("h2");
		for (var i=0; i<h2_list.length;i++){
			if (!any_img_within(h2_list.item(i))){
				h2_list.item(i).addEventListener("mouseover", blockIN, false);
				h2_list.item(i).addEventListener("mouseout", blockOUT, false);
			}
		}	
		var h3_list= window.content.document.getElementsByTagName("h3");
		for (var i=0; i<h3_list.length;i++){
			if (!any_img_within(h3_list.item(i))){
				h3_list.item(i).addEventListener("mouseover", blockIN, false);
				h3_list.item(i).addEventListener("mouseout", blockOUT, false);
			}
		}	
		var h4_list= window.content.document.getElementsByTagName("h4");
		for (var i=0; i<h4_list.length;i++){
			if (!any_img_within(h4_list.item(i))){
				h4_list.item(i).addEventListener("mouseover", blockIN, false);
				h4_list.item(i).addEventListener("mouseout", blockOUT, false);
			}
		}	
		var h5_list= window.content.document.getElementsByTagName("h5");
		for (var i=0; i<h5_list.length;i++){
			if (!any_img_within(h5_list.item(i))){
				h5_list.item(i).addEventListener("mouseover", blockIN, false);
				h5_list.item(i).addEventListener("mouseout", blockOUT, false);
			}
		}
		var h6_list= window.content.document.getElementsByTagName("h6");
		for (var i=0; i<h6_list.length;i++){
			if (!any_img_within(h6_list.item(i))){
				h6_list.item(i).addEventListener("mouseover", blockIN, false);
				h6_list.item(i).addEventListener("mouseout", blockOUT, false);
			}
		}	
		var li_list= window.content.document.getElementsByTagName("li");
		for (var i=0; i<li_list.length;i++){
			if (!any_img_within(li_list.item(i))){
				li_list.item(i).addEventListener("mouseover", blockIN, false);
				li_list.item(i).addEventListener("mouseout", blockOUT, false);
			}
		} 
 		var a_list= window.content.document.getElementsByTagName("a");
		for (var i=0; i<a_list.length;i++){
			if (!any_img_within(a_list.item(i))){
				a_list.item(i).addEventListener("mouseover", blockIN, false);
				a_list.item(i).addEventListener("mouseout", blockOUT, false);
			}
		}		
		var code_list= window.content.document.getElementsByTagName("code");
		for (var i=0; i<code_list.length;i++){
			if (!any_img_within(code_list.item(i))){
				code_list.item(i).addEventListener("mouseover", blockIN, false);
				code_list.item(i).addEventListener("mouseout", blockOUT, false);
			}
		}
		var pre_list= window.content.document.getElementsByTagName("pre");
		for (var i=0; i<pre_list.length;i++){
			if (!any_img_within(pre_list.item(i))){
				pre_list.item(i).addEventListener("mouseover", blockIN, false);
				pre_list.item(i).addEventListener("mouseout", blockOUT, false);
			}
		}
		var label_list= window.content.document.getElementsByTagName("label");
		for (var i=0; i<label_list.length;i++){
			if (!any_img_within(label_list.item(i))){
				label_list.item(i).addEventListener("mouseover", blockIN, false);
				label_list.item(i).addEventListener("mouseout", blockOUT, false);
			}
		}
		var legend_list= window.content.document.getElementsByTagName("legend");
		for (var i=0; i<legend_list.length;i++){
			if (!any_img_within(legend_list.item(i))){
				legend_list.item(i).addEventListener("mouseover", blockIN, false);
				legend_list.item(i).addEventListener("mouseout", blockOUT, false);
			}
		}
		var input_list= window.content.document.getElementsByTagName("input");
		for (var i=0; i<input_list.length;i++){
			var type_of_input=input_list.item(i).getAttribute("type");			
			if ((type_of_input=="select" || type_of_input=="radio" || type_of_input=="checkbox") && !any_img_within(input_list.item(i))){
				input_list.item(i).addEventListener("mouseover", blockIN, false);
				input_list.item(i).addEventListener("mouseout", blockOUT, false);
			}
		}
	}	
}

function any_img_within(el){
	//Check whether there is an img hanging for each a
	var el_img=el.getElementsByTagName("img");
	if (el_img.length==0) return false;//which means there is no img within a	
	else return true;
}

//=======================================================
function t7_trialanderror(){//checks the following traversal pattern A B A C A
	if (history_array.length>=((PAGE_THRHLD*2)+1)){
		var sizet7=history_array.length-1;
		if (history_array[sizet7]==history_array[sizet7-2]){
			if (history_array[sizet7-2]==history_array[sizet7-4]){
				if (history_array[sizet7-1]!=history_array[sizet7-3]){
					if (history_array[sizet7-1]!=history_array[sizet7]){//C!=A
						if (history_array[sizet7-3]!=history_array[sizet7]){//B!=A						
							if ((time_in_wp[time_in_wp.length-1]) && (time_in_wp[time_in_wp.length-3])){//check whether B and C were scans 
								//latest enacted t7 is at least 4 items away in the history
								if ((history_array.length>=last_tactic_index_t7+4) && (history_array.length>=last_tactic_index_t5+3)){ 
									if (!is_it_tabbed(4)){//non-tabbed
										if (sameDomain()){
											notify("7.1");
										}
										else if (overlappingTitle()){
											notify("7.2");
										}
										else{
											notify("7.3");
										}
									}
									else{//tabbed
										if (sameDomain()){
											notify("7.4");
										}
										else if (overlappingTitle()){
											notify("7.5");
										}
										else{
											notify("7.6");
										}
									}
									last_tactic_index_t7=history_array.length;
								}
							}
						}
					}
				}
			}
		}	
	}
}
/*KEY
 * 
 * 7.1: non-tabbed pages same domain 
 * 7.2: non-tabbed pages distinct domain, overlapping titles 
 * 7.3: non-tabbed pages distinct domain, non-overlapping titles
 * 7.4: tabbed pages same domain 
 * 7.5: tabbed pages distinct domain, overlapping titles
 * 7.6: tabbed pages distinct domain, non-overlapping title
 * 
 * */
function sameDomain(){
	var size=history_array.length-1;
	var a = window.content.document.createElement('a');
	var b = window.content.document.createElement('a');
	var c = window.content.document.createElement('a');
	a.href = history_array[size];
	b.href = history_array[size-3];
	c.href = history_array[size-1];
	if ((a.hostname==b.hostname) && (c.hostname==b.hostname) && (a.hostname==c.hostname)){//A,B and C belong to the same domain
		return true;
	}
}

function overlappingTitle(){
	var size=history_array.length-1;
	var overlap=false;
	var words=/\w*/ig;	
	var c=titles_array[size-1];
	c=c.match(words);		
	var b=titles_array[size-3];
	b=b.match(words);
	var i=0;
	while((i<=b.length-1) && (!overlap)){			
		var j=0;
		while((j<=c.length-1) && (!overlap)){			
			if (b[i]==c[j] && b[i]!=""){	
				overlap=true;
			}
			j++;
		}
		i++;
	}	
	return overlap;
}

//=======================================================
function t8_t9_restarting_quiting(){
	var closing_time=Date.now();
	Components.utils.import("resource://gre/modules/Services.jsm");
	Components.utils.import("resource://gre/modules/FileUtils.jsm");
	var file = FileUtils.getFile("ProfD", ["profile.sqlite"]);
	var dbConn = Services.storage.openDatabase(file); // Will also create the file if it does not exist
	if (last_tactic_index==history_array.length) { //if current page as triggered a problem...
		dbConn.executeSimpleSQL("UPDATE userId SET lastAccess='"+closing_time+"', troubles='"+last_tactic+"' WHERE usid='"+userID+"'");
	}
	else{
		dbConn.executeSimpleSQL("UPDATE userId SET lastAccess='"+closing_time+"', troubles= 'none' WHERE usid='"+userID+"'");
	}
	changestatus();
}

function t8_t9_restarting_checking(){
	var lastAccess=null; var endwithtrouble=null;
	var elapsed_time;
	Components.utils.import("resource://gre/modules/Services.jsm");
	Components.utils.import("resource://gre/modules/FileUtils.jsm");
	var file = FileUtils.getFile("ProfD", ["profile.sqlite"]);
	var dbConn = Services.storage.openDatabase(file); // Will also create the file if it does not exist
	var statement = dbConn.createStatement("SELECT * FROM userId");
	try{
		while (statement.executeStep()) {  
			lastAccess=statement.row.lastAccess;
			endwithtrouble=statement.row.troubles;
		}
	}
	finally{
		statement.reset(); 
	}
	if (lastAccess!=0){	//first URL but not first time usign WT	
		elapsed_time=Date.now()-lastAccess;
		if (elapsed_time<=TIME_THRHLD && endwithtrouble!="none" && endwithtrouble!="8.1" && endwithtrouble!="9.1"){//if less than one minute has elapsed and it was ended with trouble
			notify("8.1");
		}
		if (elapsed_time>=GU_TIME_THRHLD && endwithtrouble!="none" && endwithtrouble!="8.1" && endwithtrouble!="9.1"){//if more than 3 minutes have elapsed
			notify("9.1");
		}
	}
}
//=======================================================
			
// ******************************** Set up interface functionalities
function notify(tactic_flag){
	if (mode==0){//feedback mode
		alert(locale.GetStringFromName("detection"));
		switch(tactic_flag){
			case "1.1": update(tactic_flag, locale.GetStringFromName("t11"));break;//overcoming information overload
			case "1.2": update(tactic_flag, locale.GetStringFromName("t11"));break;//overcoming information overload
			case "2.1": update(tactic_flag, locale.GetStringFromName("t21"));break;//asking for help
			case "2.2": update(tactic_flag, locale.GetStringFromName("t21"));break;//asking for help
			case "2.3": update(tactic_flag, locale.GetStringFromName("t21"));break;//asking for help
			case "2.4": update(tactic_flag, locale.GetStringFromName("t21"));break;//asking for help
			case "2.5": update(tactic_flag, locale.GetStringFromName("t21"));break;//asking for help
			case "2.6": update(tactic_flag, locale.GetStringFromName("t21"));break;//asking for help
			case "3.1": update(tactic_flag, locale.GetStringFromName("t31"));break;//sheltering
			case "4.1": update(tactic_flag, locale.GetStringFromName("t41"));break;//retracing
			case "4.2": update(tactic_flag, locale.GetStringFromName("t41"));break;//retracing with backtracking
			case "5.1": update(tactic_flag, locale.GetStringFromName("t51"));break;//persevering-->disbelieving
			case "5.2": update(tactic_flag, locale.GetStringFromName("t51"));break;//persevering-->disbelieving
			case "6.1": update(tactic_flag, locale.GetStringFromName("t61"));break;//nop
			case "7.1": update(tactic_flag, locale.GetStringFromName("t71"));break;//trial and errors
			case "7.2": update(tactic_flag, locale.GetStringFromName("t71"));break;//trial and errors
			case "7.3": update(tactic_flag, locale.GetStringFromName("t71"));break;//trial and errors
			case "7.4": update(tactic_flag, locale.GetStringFromName("t71"));break;//trial and errors
			case "7.5": update(tactic_flag, locale.GetStringFromName("t71"));break;//trial and errors
			case "7.6": update(tactic_flag, locale.GetStringFromName("t71"));break;//trial and errors
			case "8.1": update(tactic_flag, locale.GetStringFromName("t81"));break;//restarting
			case "9.1": update(tactic_flag, locale.GetStringFromName("t91"));break;//giving up
			default:break;
		}
	}
	else if(mode==1){//test mode
		printonsidebar(tactic_flag+" detected in "+currentURL);
		last_tactic=tactic_flag;
		last_tactic_index=history_array.length;
		nots_responded++;nots_prompted++;
	}
	else if(mode==2){//silent mode
		nots_responded++;//nots_prompted will be equals 0 to distinguish this mode in the DB
		last_tactic=tactic_flag;
		last_tactic_index=history_array.length;
		send_info_silent(tactic_flag);
	}
}

function send_info_silent(tactic){//we assume a tactic was employed
	var URL=escape(currentURL);
	var grand_tactic=tactic.substring(0,1);
	if ((grand_tactic=="2")||(grand_tactic=="7")){//get A, B, C
		urlA=history_array[history_array.length-1];urlA=escape(urlA);
		urlB=history_array[history_array.length-4];urlB=escape(urlB);
		urlC=history_array[history_array.length-2];urlC=escape(urlC);
	}
	if (grand_tactic=="5"){//get A, B
		urlA=history_array[history_array.length-2];urlA=escape(urlA);
		urlB=history_array[history_array.length-1];urlB=escape(urlB);
	}
	var requestObject = new XMLHttpRequest();
	requestObject.open("POST",SERVER_TACTICS,true);
	requestObject.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	
	if ((grand_tactic=="2")||(grand_tactic=="7")){//get A, B, C
		requestObject.send("id="+userID+"&vs="+VERSION+"&tactic="+tactic+"&URL="+URL+"&op=true&res=true&com=silent&A="+urlA+"&B="+urlB+"&C="+urlC); 
	}
	else if (grand_tactic=="5"){
		requestObject.send("id="+userID+"&vs="+VERSION+"&tactic="+tactic+"&URL="+URL+"&op=true&res=true&com=silent&A="+urlA+"&B="+urlB); 				
	}
	else{
		requestObject.send("id="+userID+"&vs="+VERSION+"&tactic="+tactic+"&URL="+URL+"&op=true&res=true&com=silent");			
	}	
}


function submit_session(){
	var requestObject = new XMLHttpRequest(); 
	requestObject.open("POST",SERVER_STATS,false);
	requestObject.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	if (mode==0)//feedback mode
		requestObject.send("id="+userID+"&vs="+VERSION+"&prompts="+nots_prompted+"&answers="+nots_responded+"&start="+session_started+"&end="+session_ended);
	else
		requestObject.send("id="+userID+"&vs="+VERSION+"&prompts="+nots_prompted+"&answers="+nots_responded+"&start="+session_started+"&end="+session_ended);
}

function update(tactic, text){
	//Design the key
	var key=new String(tactic);
	var pagetitle=content.document.title;		
		
	if (pagetitle.length>60){
		pagetitle1=pagetitle.substring(0,20);
		pagetitle2=pagetitle.substring(pagetitle.length-20,pagetitle.length);
		pagetitle=pagetitle1+"[...]"+pagetitle2;
	}
	//end "Design the key"
	nots_prompted++;
	if (document.getElementById(key)){
		var time=new Date;
		var tmptext=document.getElementById(key).childNodes[0].childNodes[0];
		var hh0=time.getHours().toString(); var mm0=time.getMinutes().toString(); var ss0=time.getSeconds().toString();
		if (hh0.length==1) hh0="0"+hh0;
		if (mm0.length==1) mm0="0"+mm0;
		if (ss0.length==1) ss0="0"+ss0;
		tmptext.textContent=pagetitle+" ["+hh0+":"+mm0+":"+ss0+"] ";
	}
	else{
		var hboxURL= document.createElement("hbox");
		var hbox0 = document.createElement("hbox");
		var hbox1 = document.createElement("hbox");
		var groupbox = document.createElement("groupbox");
		var label = document.createElement("description");
		var labelURL=document.createElement("description");
		var button1 = document.createElement("button");
		var button2 = document.createElement("button");
		var time=new Date;
		groupbox.setAttribute("id",key);
		var hh=time.getHours().toString(); var mm=time.getMinutes().toString(); var ss=time.getSeconds().toString();
		if (hh.length==1) hh="0"+hh;
		if (mm.length==1) mm="0"+mm;
		if (ss.length==1) ss="0"+ss;
		//label.setAttribute("value", "["+hh+":"+mm+":"+ss+"] "+text);
		label.appendChild(document.createTextNode(text));
		label.setAttribute("class","q"); 
		labelURL.appendChild(document.createTextNode(pagetitle+" ["+hh+":"+mm+":"+ss+"]"));
		labelURL.setAttribute("class","head");
		button1.setAttribute("label",locale.GetStringFromName("y"));
		button1.setAttribute("flex","1");
		button1.setAttribute("oncommand","collectresponse(\'"+key+"\',"+true+")");
		button1.setAttribute("disabled","false");
		button2.setAttribute("label",locale.GetStringFromName("n"));
		button2.setAttribute("flex","1");
		button2.setAttribute("oncommand","collectresponse(\'"+key+"\',"+false+")");
		button1.setAttribute("disabled","false");
		hboxURL.appendChild(labelURL);
		hbox0.appendChild(label);
		hbox1.appendChild(button1);
		hbox1.appendChild(button2);
		groupbox.appendChild(hboxURL);	
		groupbox.appendChild(hbox0);
		groupbox.appendChild(hbox1);
		event_panel.appendChild(groupbox);
	}
}

function collectresponse(key, button){
	//disable button
	var groupbox=document.getElementById(key);
	//disable buttons
	groupbox.childNodes[2].childNodes[0].setAttribute("disabled", "true");
	groupbox.childNodes[2].childNodes[1].setAttribute("disabled", "true");
	if (button){
		//variables-control declaration
		var hbox2 = document.createElement("hbox");
		var hbox21 = document.createElement("hbox");
		var button1 = document.createElement("button");
		var button2 = document.createElement("button");
		button1.setAttribute("label",locale.GetStringFromName("y"));
		button1.setAttribute("flex","1");
		button2.setAttribute("label",locale.GetStringFromName("n"));
		button2.setAttribute("flex","1");
		button1.setAttribute("oncommand","secondary_response(\'"+key+"\',"+true+")");
		button2.setAttribute("oncommand","secondary_response(\'"+key+"\',"+false+")");
		var secondary_txt = document.createElement("description");
		secondary_txt.setAttribute("class","q"); 
		var tactic=key.substring(0,3);
		switch(tactic){
			case "1.1": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c11")));break;//overcoming information overload
			case "1.2": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c11")));break;//overcoming information overload
			case "2.1": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c21")));break;//asking for help
			case "2.2": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c21")));break;//asking for help
			case "2.3": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c21")));break;//asking for help
			case "2.4": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c21")));break;//asking for help
			case "2.5": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c21")));break;//asking for help
			case "2.6": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c21")));break;//asking for help
			case "3.1": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c31")));break;//sheltering
			case "4.1": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c41")));break;//retracing
			case "4.2": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c41")));break;//retracing
			case "5.1": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c51")));break;//persevering
			case "5.2": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c51")));break;//persevering
			case "6.1": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c61")));break;//nop
			case "7.1": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c71")));break;//trial and errors
			case "7.2": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c71")));break;//trial and errors
			case "7.3": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c71")));break;//trial and errors
			case "7.4": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c71")));break;//trial and errors
			case "7.5": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c71")));break;//trial and errors
			case "7.6": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c71")));break;//trial and errors
			case "8.1": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c81")));break;//restarting
			case "9.1": secondary_txt.appendChild(document.createTextNode(locale.GetStringFromName("c91")));break;//giving up
			default:break;
		}
		hbox2.appendChild(secondary_txt);
		hbox21.appendChild(button1);
		hbox21.appendChild(button2);
		groupbox.appendChild(hbox2);
		groupbox.appendChild(hbox21);
	}
	//controls of the textarea common to both options
	var hbox3 = document.createElement("hbox");
	var hbox4 = document.createElement("hbox");
	var textarea = document.createElement("textbox");
	var tarea_but = document.createElement("button");
	textarea.setAttribute("id","");
	textarea.setAttribute("value",locale.GetStringFromName("extra"));
	textarea.setAttribute("onclick","clean(\'"+key+"\',"+button+")");
	textarea.setAttribute("multiline",true);
	textarea.setAttribute("flex","1");
	textarea.setAttribute("rows",2);
	tarea_but.setAttribute("label",locale.GetStringFromName("submit"));
	tarea_but.setAttribute("flex", "2");
	tarea_but.setAttribute("disabled","true");
	tarea_but.setAttribute("oncommand","send_info(\'"+key+"\',"+button+")");
	hbox3.appendChild(textarea);
	hbox4.appendChild(tarea_but);
	groupbox.appendChild(hbox3);
	groupbox.appendChild(hbox4);
}

function send_info(key,button){// removing nodes from http://www.w3schools.com/dom/dom_nodes_remove.asp
		var urlA=null;var urlB=null;var urlC=null;var comments;var sec_res=null;
		var groupbox=document.getElementById(key);
		//AJAX communication with server - Good AJAX tutorial in http://www.impressivewebs.com/ajax-from-the-ground-up-part-1-xmlhttprequest/
		var requestObject = new XMLHttpRequest();
		requestObject.open("POST",SERVER_TACTICS,true);
		requestObject.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var tactic=key.substring(0,3);
		var grand_tactic=key.substring(0,1);
		var URL=escape(currentURL);// escape & and similar for the AJAX call
		if ((grand_tactic=="2")||(grand_tactic=="7")){//get A, B, C
			urlA=history_array[history_array.length-1];urlA=escape(urlA);
			urlB=history_array[history_array.length-4];urlB=escape(urlB);
			urlC=history_array[history_array.length-2];urlC=escape(urlC);
		}
		if (grand_tactic=="5"){//get A, B
			urlA=history_array[history_array.length-2];urlA=escape(urlA);
			urlB=history_array[history_array.length-1];urlB=escape(urlB);
		}
		//different submissions for different tactics
		if (button){// if the prediction is right submit also 2ndary response
			sec_res=groupbox.childNodes[4].childNodes[0].getAttribute("value");
			comments=groupbox.childNodes[5].childNodes[0].value;
			last_tactic=tactic;
			last_tactic_index=history_array.length;
		}
		else{
			comments=groupbox.childNodes[3].childNodes[0].value;	
		}
		comments=comments.replace("'"," ");
		comments=escape(comments);		
		if ((grand_tactic=="2")||(grand_tactic=="7")){//get A, B, C
			requestObject.send("id="+userID+"&vs="+VERSION+"&tactic="+tactic+"&URL="+URL+"&op="+button+"&res="+sec_res+"&com="+comments+"&A="+urlA+"&B="+urlB+"&C="+urlC); 
		}
		else if (grand_tactic=="5"){
			requestObject.send("id="+userID+"&vs="+VERSION+"&tactic="+tactic+"&URL="+URL+"&op="+button+"&res="+sec_res+"&com="+comments+"&A="+urlA+"&B="+urlB); 				
		}
		else{
			requestObject.send("id="+userID+"&vs="+VERSION+"&tactic="+tactic+"&URL="+URL+"&op="+button+"&res="+sec_res+"&com="+comments);			
		}
		nots_responded++;
		groupbox.parentNode.removeChild(groupbox);
	}

function clean(key, button){
		var groupbox=document.getElementById(key);
		if (button){			
			var tmp=groupbox.childNodes[5].childNodes[0].getAttribute("value");
			if (tmp==locale.GetStringFromName("extra")){
				var secondbutton_clicked=groupbox.childNodes[4].childNodes[0].getAttribute("value");
				if ((secondbutton_clicked=="true") || (secondbutton_clicked=="false")){
					groupbox.childNodes[5].childNodes[0].setAttribute("value","");
					groupbox.childNodes[6].childNodes[0].setAttribute("disabled","false");
				}
			}			
		}
		else{
			var tmp=groupbox.childNodes[3].childNodes[0].getAttribute("value");
			if (tmp==locale.GetStringFromName("extra"))
				groupbox.childNodes[3].childNodes[0].setAttribute("value","");
			groupbox.childNodes[4].childNodes[0].setAttribute("disabled","false");
		}
}

function secondary_response(key, button){
		var groupbox=document.getElementById(key);
		groupbox.childNodes[4].childNodes[0].setAttribute("disabled","true");
		groupbox.childNodes[4].childNodes[1].setAttribute("disabled","true");
		if (button)
			groupbox.childNodes[4].childNodes[0].setAttribute("value","true");
		else
			groupbox.childNodes[4].childNodes[0].setAttribute("value","false");
		var tmp=groupbox.childNodes[5].childNodes[0].getAttribute("value");
		if (tmp!=locale.GetStringFromName("extra")){
			groupbox.childNodes[6].childNodes[0].setAttribute("disabled","false");
		}
}

//********************************  helping methods ******************************** 
function randomString(length) {
    var chars = 'abcdefghiklmnopqrstuvwxyz'.split('');
    var str='';
    for (var i = 0; i < length; i++)
        str += chars[Math.floor(Math.random() * chars.length)];
    return str;
}

function retrieveText(element) {
    var text= [];
    for (var i= 0, n= element.childNodes.length; i<n; i++) {
        var child= element.childNodes[i];
        if (child.nodeType===1 && child.nodeName.toLowerCase()!=="script" && child.nodeName.toLowerCase()!=="style" && child.nodeName.toLowerCase()!=="select" ){
        	 var style=window.getComputedStyle(child);
        	 if ((style.getPropertyValue("display") == "none") || (style.getPropertyValue("visibility") == "hidden") ||(style.getPropertyValue("left") < 0)) {
             }
         	 else{//the recursive call will only be made on those elements that are visible
         		 text.push(retrieveText(child));
         	 }
        }
        else if (child.nodeType===3){
            text.push(child.data);
        }
    }
    return text.join(' ');
}

function visiblewords(){
	var no_of_words_all=0;
	var no_of_words_h=0;
	if (window.content.document.URL=="about:blank" || window.content.document.URL=="about:newtab")
		return 0;
	if (document.location==null)
		return 0;
	else{
		/* get the number of all words*/
		var re_blanks=/\s{2,}/ig;
		var w_per_sentence=/\s+?/ig;
		var re=/<.*>/ig;
		var html_string=retrieveText(window.content.document.body);
		html_string=html_string.replace(re," ");
		html_string=html_string.replace(re_blanks," ");
		no_of_words_all=html_string.split(w_per_sentence).length;
		
		/* get the number of words in headings*/
		var html_string=window.content.document.body.innerHTML;
		var h_re=/<h\d{1}[\w\W\s]*?\/h\d{1}>/ig;
		var re2=/>[\s\w\s]*</ig;
		var words_and_digits=/\w*/ig;		
		html_string=html_string.match(h_re);
		if (html_string!=null){
			var hxstring=html_string.join();
			hxstring=hxstring.match(re2);
			if (hxstring!=null){
				var wd=hxstring.join();
				wd=wd.match(words_and_digits);
				for (var i=0; i< wd.length;i++){
					if (wd[i]==""){}
					else{no_of_words_h++;}
				}
			}
			else{no_of_words_h=0;}
		}else{no_of_words_h=0;}
		
		if (no_of_words_h==0 && no_of_words_all!=0){//there is no header content but there is textual content
			header_text=false;	
			return no_of_words_all;
		}
		else if (no_of_words_h>WPS*CORR_VALUE_H){
			header_text=true;
			return no_of_words_h;
		}
		else{
			header_text=false;	
			return no_of_words_all;
		}
	}
}

function time_management(){
	if (status){
		nav_time= (Date.now()-start)/1000;		
		if (arrival_mode=="updated" && (nav_time<0.5)){//detect url redirects
			history_array.splice(history_array.length-1, 1);//remove the current page			
			traversal_array.splice(traversal_array.length-1, 1);
			landing_mode.splice(landing_mode.length-1, 1);
			url_array.splice(url_array.length-1, 1);
			titles_array.splice(titles_array.length-1, 1);
		}
		else{
			urlsunloaded++;
			setTimeSpent(nav_time);
			var words_per_sec=NUMBER_OF_WORDS/nav_time;
			words_per_sec=words_per_sec.toFixed(0);
			if (words_per_sec>page_ratio) time_in_wp.push(true);		
			//a regular browsing
			else time_in_wp.push(false);
			last_t_added_URL=currentURL;//last unloaded page
		}
	}
}
// set time spent in profile.sqlite
function setTimeSpent(t){
	var total_time;
	var time_sofar;
	var keyWT;
	Components.utils.import("resource://gre/modules/Services.jsm");
	Components.utils.import("resource://gre/modules/FileUtils.jsm");
	var file = FileUtils.getFile("ProfD", ["profile.sqlite"]);
	var dbConn = Services.storage.openDatabase(file); // Will also create the file if it does not exist
	dbConn.executeSimpleSQL("CREATE TABLE IF NOT EXISTS historyWT (url STRING, keyWT FLOAT, timespent FLOAT, frecencyWT FLOAT)");
	var fcy=getFrecency();
	var statement = dbConn.createStatement("SELECT timespent FROM historyWT where url=='"+currentURL+"'");
	var emptyDB = true; 	
	try{
		while (statement.executeStep()) {  
			emptyDB = false;
			time_sofar=statement.row.timespent;
			total_time=time_sofar+t;
			keyWT=total_time*fcy;
			dbConn.executeSimpleSQL("UPDATE historyWT SET keyWT ='"+keyWT+"',timespent ='"+total_time+"',frecencyWT='"+fcy+"'  WHERE url=='"+currentURL+"'"); 
		}
		if (emptyDB){//this url is not in the database and the time spent in current is introduced
			keyWT=t*fcy;
			dbConn.executeSimpleSQL("INSERT INTO historyWT VALUES ('"+currentURL+"','"+keyWT+"','"+t+"','"+fcy+"')");
		}
	}
	finally{
		statement.reset(); 
	}
	
}

function getFrecency(){
	//update database for sheltering
	var fcy=null;
	Components.utils.import("resource://gre/modules/Services.jsm");
	Components.utils.import("resource://gre/modules/FileUtils.jsm");
	var file = FileUtils.getFile("ProfD", ["places.sqlite"]);
	var dbConn = Services.storage.openDatabase(file); // Will also create the file if it does not exist
	var statement = dbConn.createStatement("SELECT frecency FROM moz_places WHERE url=='"+currentURL+"'");
	var emptyDB = true;
	try{
		while (statement.executeStep()) {  
			emptyDB = false;
			fcy=statement.row.frecency;
		}
		if (emptyDB){
			fcy=1;
		}
	}
	finally{
		statement.reset(); 
	}
	return fcy;
}


function manageUserId(){
	Components.utils.import("resource://gre/modules/Services.jsm");
	Components.utils.import("resource://gre/modules/FileUtils.jsm");
	var file = FileUtils.getFile("ProfD", ["profile.sqlite"]);
	var dbConn = Services.storage.openDatabase(file); // Will also create the file if it does not exist
	dbConn.executeSimpleSQL("CREATE TABLE IF NOT EXISTS userId (usid STRING, lastAccess STRING, troubles STRING)");
	var statement = dbConn.createStatement("SELECT * FROM userId");
	var emptyDB = true; 	
	try{
		while (statement.executeStep()) {  
			emptyDB = false;
			userID=statement.row.usid;
		}
		if (emptyDB){
			userID=randomString(4)+Date.now();
			dbConn.executeSimpleSQL("INSERT INTO userId VALUES ('"+userID+"', '0', 'start')");
		}
	}
	finally{
		statement.reset(); 
	}
}

function managebreaks(){
	start_stop();
	window.document.getElementsByTagName("page")[0].setAttribute("delayed","true");
	alert(locale.GetStringFromName("logout"));	
}

//____function for debugging purposes____//
function printonsidebar(s){
	var l1 = document.createElement("label");
	l1.setAttribute("value",s);
	event_panel=window.document.getElementById("eventpanel");
	event_panel.appendChild(l1);
}

function start_stop(){
	var wt_ui=window.document.getElementsByTagName("page")[0];
	var hidden=wt_ui.getAttribute("hidden");
	var delayed=wt_ui.getAttribute("delayed");
	if (delayed){//cancel any delaying request
		wt_ui.setAttribute("delayed","false");
	}
	if (hidden=="true"){//if it is hidden, set it visible
		wt_ui.setAttribute("hidden","false");
		changestatus();
	}
	else if (hidden=="false"){
		if (status){//if it is not hidden and is running, turn it off and set it invisible
			changestatus();
		}
		wt_ui.setAttribute("hidden","true");
	}
}